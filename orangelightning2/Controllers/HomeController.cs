﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Orange_Lightning.Controllers
{
    /*
    color orange to use
    #ff8c1a
    rgb(255, 140, 26)
    hsl(30, 100%, 55%)
    hsv(30, 90%, 100%)
    */
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Status()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Portfolio()
        {
            return View();
        }
    }
}